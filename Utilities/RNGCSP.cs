﻿using System;
using System.Security.Cryptography;

namespace DMGMAlly
{
    /// <summary>
    /// A class to generate a crypto random number
    /// </summary>
    public class DiceRoller
    {
        /// <summary>
        /// Returns a random number between start and end inclusive.
        /// </summary>
        /// <param name="start">The lowest number acceptable</param>
        /// <param name="end">The highest number acceptable</param>
        /// <returns>Int32 random number between start and end</returns>
        public static int RollDice(int start, int end)
        {
            return Roll(start, end + 1);
        }

        /// <summary>
        /// Returns a random number between 1 and the number of sides on the die, inclusive.
        /// </summary>
        /// <param name="numberSides">The number of sides on the die.</param>
        /// <returns>Int32 random number between 1 and the number of sides on the die.</returns>
        public static int RollDice(int numberSides)
        {
            return Roll(1, numberSides + 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private static int Roll(int start, int end)
        {
            return RandomNumberGenerator.GetInt32(start, end + 1);
        }
    }
}
